﻿using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;

public class DataManager : MonoBehaviour {

    public static string name;
    public static float cost;
    public static string description;
    public static string supplier;

    public static IDbConnection dbconn;
    public static IDbCommand dbcmd;




    public static void ReadSQL(string s)
    {
        try
        {
            string conn = "URI=file:" + Application.streamingAssetsPath + "/Data/Data.db"; //Path to database.

            dbconn = (IDbConnection)new SqliteConnection(conn);
            dbconn.Open();
            dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT Name, Cost, Description, Supplier " + "FROM DATA";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {

                name = reader.GetString(0);
                //int pCode = reader.GetInt32(1);
                cost = reader.GetFloat(1);
                description = reader.GetString(2);
                supplier = reader.GetString(3);

                if (name == s)
                {
                    VisManager._instance.DisplayText(name, cost, description, supplier);
                    Debug.Log("Product Name: " + name + " Cost: " + cost + " Description: " + description + " Supplier: " + supplier);
                }

                //Debug.Log("Product Name: " + name + " Cost: " + cost + " Description: " + description + " Supplier: " + supplier);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            dbconn.Close();
            dbconn = null;
        }
        catch (Exception c)
        {
            Debug.Log("Unable to connect to DB, or Database empty " + c.ToString());
        }
    }
}
