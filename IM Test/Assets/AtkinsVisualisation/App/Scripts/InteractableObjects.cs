﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InteractableObjects : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler
{
    private GameObject selectedGameObject;
    private GameObject previousGameObject;
    private Color defaultColor;



    void Start()
    {
        defaultColor = this.gameObject.GetComponent<Renderer>().material.color;
        VisManager._instance.GetDefaultColor(defaultColor);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        selectedGameObject = this.gameObject;
        VisManager.HighlightGameObject(true, this.gameObject, Color.blue);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        DataManager.ReadSQL(this.gameObject.name);
        VisManager.HighlightGameObject(true, this.gameObject, Color.green);
        VisManager._instance.SelectedGameObject(true, this.gameObject);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        VisManager.HighlightGameObject(true, this.gameObject, defaultColor);
        VisManager._instance.SelectedGameObject(false, this.gameObject);
        previousGameObject = selectedGameObject;
    }
}
