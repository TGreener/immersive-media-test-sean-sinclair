﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisManager : MonoBehaviour {

    public static VisManager _instance = null;
    public GameObject button;
    public Text productText;

    private string textDisplay;
    private static Color defaultColor;

    // Singleton Pattern, not using Dependency Injection as it is a small project and will increase complexity then it needs to be.
    void Awake()
    {
        if (_instance == null)
            _instance = this;

        else if (_instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);

    }

    public void DisplayText(string N, float C, string D, string S)
    {
        textDisplay = "Product Name: " + N  + " \r\n  Product cost: " +  C + " \r\n Product Description: \r\n " + " " + D + " \r\n Supplier: " + S;
        productText.text = textDisplay;
    }

    public void GetDefaultColor(Color Col)
    {
        defaultColor = Col;
    }

    public void SelectedGameObject(bool B, GameObject G)
    {
        if (B)
        {
            // Do something
        }
        else
        {
            productText.text = "";
        }
    }
    //unused
    /*public static Vector3 SpawnUI(bool B, GameObject G)
    {
        return SpawnUI(B, G);
    }*/

    public static void SpawnUI(bool B, GameObject G, Transform T)
    {
        if (B)
        {
            Instantiate(G, T);
        }
        else
        {
            Destroy(G);
        }
    }

    public static GameObject HighlightGameObject(bool B, GameObject G)
    {
        return HighlightGameObject(B, CustomGameObject.ReturnedGameObject(), CustomColor.ReturnedColor());
    }


    public static GameObject HighlightGameObject(bool B, GameObject G, Color C)
    {
        if (B && C != null)
        {
            G.GetComponent<Renderer>().material.color = C;
        }
        else if (!B)
        {
            //G.GetComponent<Renderer>().material.color = Color.grey;
            G.GetComponent<Renderer>().material.color = defaultColor;
        }

        return null;
    }

    public void ExitApplication()
    {
        Application.Quit();
    }
}
